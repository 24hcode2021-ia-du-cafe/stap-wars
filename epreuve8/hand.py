import math, cv2, time, requests
import numpy as np

import mediapipe as mp

SERVER_ADRESS = "http://192.168.72.1:8080" # don't forget port
MODE_FREE = True # Use free moving or False to only use 4 directions
 
def print_direction(deg:float):
    if deg < -135.0 :
        print("Droite")
    elif deg < -45.0:
        print("Haut")
    elif deg < 45.0:
        print("Gauche")
    elif deg < 135.0:
        print("Bas")
    else :
        print("Droite")

def send_direction(deg:float):
    if MODE_FREE :
        requests.post(SERVER_ADRESS, json={"angle":deg})
        return

    if deg < -135.0 :
        requests.get(SERVER_ADRESS+"/droite")
    elif deg < -45.0:
        requests.get(SERVER_ADRESS+"/haut")
    elif deg < 45.0:
        requests.get(SERVER_ADRESS+"/gauche")
    elif deg < 135.0:
        requests.get(SERVER_ADRESS+"/bas")
    else :
        requests.get(SERVER_ADRESS+"/droite")

mp_hands = mp.solutions.hands
mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles

# Use OpenCV to capture video frames
cap = cv2.VideoCapture(0)
cap.set(3, 640)
cap.set(4, 480)

# TODO add a true app killer :D
while True: 
    #time.sleep(0.1)

    # Read image frame from video capture
    success, img = cap.read()
    img = cv2.flip(img, 1)
    imgResult = img.copy()

    # Detect hands using mediapipe library : https://google.github.io/mediapipe/
    # Don't hesitate to fine tune params, especialy min_detection_confidence
    with mp_hands.Hands(
            static_image_mode=True,
            max_num_hands=2,
            min_detection_confidence=0.8) as hands:
        results = hands.process(cv2.flip(cv2.cvtColor(img, cv2.COLOR_BGR2RGB), 1))

        if results.multi_hand_landmarks:
            image_hight, image_width, _ = img.shape
            annotated_image = cv2.flip(img.copy(), 1)
            for hand_landmarks in results.multi_hand_landmarks:
                mp_drawing.draw_landmarks(
                    annotated_image,
                    hand_landmarks,
                    mp_hands.HAND_CONNECTIONS,
                    mp_drawing_styles.get_default_hand_landmarks_style(),
                    mp_drawing_styles.get_default_hand_connections_style())
                imgResult = cv2.flip(annotated_image, 1)

                # Get degree orientation from hand detected points. See more about those points here : https://google.github.io/mediapipe/solutions/hands.html#hand-landmark-model
                deg = math.degrees(math.atan2(hand_landmarks.landmark[12].y - hand_landmarks.landmark[0].y, hand_landmarks.landmark[12].x - hand_landmarks.landmark[0].x))
            
                # print_direction(deg)
                send_direction(deg)

    
    # displaying output on Screen
    cv2.imshow("Result", imgResult)
       
    # condition to break programs execution
    # press q to stop the execution of program
    if cv2.waitKey(1) and 0xFF == ord('q'): 
        break

#         MAY THE FORCE BE WITH YOU, JEDI
#                     ____
#                  _.' :  `._
#              .-.'`.  ;   .'`.-.
#     __      / : ___\ ;  /___ ; \      __
#   ,'_ ""--.:__;".-.";: :".-.":__;.--"" _`,
#   :' `.t""--.. '<@.`;_  ',@>` ..--""j.' `;
#        `:-.._J '-.-'L__ `-- ' L_..-;'
#          "-.__ ;  .-"  "-.  : __.-"
#              L ' /.------.\ ' J
#               "-.   "--"   .-"
#              __.l"-:_JL_;-";.__
#           .-j/'.;  ;""""  / .'\"-.
#         .' /:`. "-.:     .-" .';  `.
#      .-"  / ;  "-. "-..-" .-"  :    "-.
#   .+"-.  : :      "-.__.-"      ;-._   \
#   ; \  `.; ;                    : : "+. ;
#   :  ;   ; ;                    : ;  : \:
#  : `."-; ;  ;                  :  ;   ,/;
#   ;    -: ;  :                ;  : .-"'  :
#   :\     \  : ;             : \.-"      :
#    ;`.    \  ; :            ;.'_..--  / ;
#    :  "-.  "-:  ;          :/."      .'  :
#      \       .-`.\        /t-""  ":-+.   :
#       `.  .-"    `l    __/ /`. :  ; ; \  ;
#         \   .-" .-"-.-"  .' .'j \  /   ;/
#          \ / .-"   /.     .'.' ;_:'    ;
#           :-""-.`./-.'     /    `.___.'
#                 \ `t  ._  / 
#                  "-.t-._:'



