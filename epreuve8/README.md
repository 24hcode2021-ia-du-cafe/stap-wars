## Bon je suis vraiment très fatigué !

# How to install :

Use python and pip to install dependencies in `requirement.pip` with :
```
pip install -r requirement.pip
```

* `hand.py` contains the client code to detect hand orientation (open hand, from bottom to middle finger).
* `server.py` contains the server needs to be push on the micro controler.

Don't forget to check IP adresses and port in `hand.py` and `server.py`

You can also draw a maze then play with your hand (but it's quite long and not totally implemented)