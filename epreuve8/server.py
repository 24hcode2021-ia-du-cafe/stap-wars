#!/usr/bin/env python3
"""
Very simple HTTP server in python for logging requests
Usage::
    ./server.py [<port>]
"""
from http.server import BaseHTTPRequestHandler, HTTPServer
import logging

import sys, json, math
sys.path.append('../lib_xwing')
from lib_xwing import XWing


class S(BaseHTTPRequestHandler):
    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        # logging.info("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))
        self._set_response()
        self.wfile.write("GET request for {}".format(self.path).encode('utf-8'))

        print(type(self.path))
        if str(self.path).__contains__("haut"):
            xwing.goto(xwing.x, xwing.y -4)
        elif str(self.path).__contains__("bas"):
             xwing.goto(xwing.x, xwing.y +4)
        elif str(self.path).__contains__("gauche"):
             xwing.goto(xwing.x -4, xwing.y)             
        elif str(self.path).__contains__("droite"):
             xwing.goto(xwing.x +4, xwing.y)
        elif str(self.path).__contains__("home"):
             xwing.goto(xwing.CENTER_WIDTH, xwing.CENTER_HEIGHT)
        #elif str(self.path).contains("home"):
        #     xwing.goto(xwing.CENTER_WIDTH, xwing.CENTER_HEIGHT)             
            
             
             
    def do_POST(self):
        xwing.pen_down()
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself

        # logging.info("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n",
        #         str(self.path), str(self.headers), post_data.decode('utf-8'))
        
        data = json.loads(post_data.decode('utf-8'))
        angle = math.radians(data["angle"])
        speed = 4
        xwing.draw_line(xwing.x - speed * math.cos(angle), xwing.y +  speed* math.sin(angle))
        
        self._set_response()
        self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))

def run(server_class=HTTPServer, handler_class=S, port=8080):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')

if __name__ == '__main__':
    from sys import argv

    xwing = XWing()

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
