#include <stdlib.h>
#include <stdio.h>    /* printf() */

#include "draw.h"

#define SQUARE_SIZE_MM        450.0

int main(int argc, char *argv[]) {
	float drawing_area_x0, drawing_area_y0, drawing_area_w, drawing_area_h;
	float x, y;

	printf("Here we go...\n");

	draw_init();
	printf("Press ENTER when done\n");
	getchar();

	draw_get_drawing_area(&drawing_area_x0, &drawing_area_y0, &drawing_area_w, &drawing_area_h);
	printf("X-Wing drawing area (mm): (%.2f, %.2f) %.2f x %.2f\n",
		   drawing_area_x0, drawing_area_y0, drawing_area_w, drawing_area_h);

	/* Move X-Wing at the square top-left corner in the drawing area */
	x = (drawing_area_w - SQUARE_SIZE_MM) / 2.0;
	y = (drawing_area_h - SQUARE_SIZE_MM) / 2.0;
	printf("draw_goto (%.2f, %.2f)\n", x, y);
	draw_goto(x, y);

	/* Draw the square */
	draw_line(x + SQUARE_SIZE_MM, y);
	draw_line(x + SQUARE_SIZE_MM, y + SQUARE_SIZE_MM);
	draw_line(x, y + SQUARE_SIZE_MM);
	draw_line(x, y);

	draw_home();

	printf("Press ENTER to quit\n");
	getchar();

	draw_close();

	printf("Great! Bye...\n");

	return 0;
}
