from svgpathtools import svg2paths2, Path, Line, CubicBezier, Arc, QuadraticBezier, Document


class svgFile:
    def __init__(self, filepath: str):
        #self.paths, self.attributes, self.svg_attributes = svg2paths2(filepath)
        self.paths = Document(filepath).paths()

    def __convert_line__(self, line: Line, scale=1.0):
        '''
        Convert a line to a list of 4 coordinates

        Returns
        -------
        A list of list of 4 values (of type float)
        x coordinate of the first point
        y coordinate of the first point
        x coordinate of the second point
        y coordinate of the second point
        '''
        x1 = line.start.real * scale
        y1 = line.start.imag * scale
        x2 = line.end.real * scale
        y2 = line.end.imag * scale
        return [[x1, y1, x2, y2]]

    def __convert_bezier__(self, bezier: CubicBezier, scale=1.0):
        '''
        Convert a line to a list of 4 coordinates

        Returns
        -------
        A list of list of 4 values (of type float)
        x coordinate of the first point
        y coordinate of the first point
        x coordinate of the second point
        y coordinate of the second point
        '''
        # Set the number of samples to 10
        # TODO : calculate the best number of sample depending on the 
        #        length of the Bezier line
        #print(f'Path length = {path.length()}')
        NUM_SAMPLES = 10

        # Build a list of points on the bezier curve
        list_points = []
        for i in range(NUM_SAMPLES):
            list_points.append(bezier.point(i/(NUM_SAMPLES-1)))

        # Build a list of lines from the points
        from_point = list_points[0]
        list_lines = []
        for i in range(1, len(list_points)):
            to_point = list_points[i]
            x1 = from_point.real * scale
            y1 = from_point.imag * scale
            x2 = to_point.real * scale
            y2 = to_point.imag * scale
            list_lines.append([x1, y1, x2, y2])
            from_point = to_point
        return list_lines

    def __convert_arc__(self, arc: Arc, scale=1.0):
        return self.__convert_bezier__(arc, scale=scale)

    def __convert_Quadratic_bezier__(self, bezier: QuadraticBezier, scale=1.0):
        return self.__convert_bezier__(bezier, scale=scale)

    def convert_to_list(self, scale=1.0):
        list_lines = []
        for path in self.paths:
            # If path is void then do nothing
            if path == Path():
                print('WARNING : found an empty Path, skipping...')
                continue
            for item in path:
                if type(item) == Line:
                    line_to_draw = self.__convert_line__(item, scale=scale)
                    list_lines += line_to_draw
                elif type(item) == CubicBezier:
                    line_to_draw = self.__convert_bezier__(item, scale=scale)
                    list_lines += line_to_draw
                elif type(item) == Arc:
                    line_to_draw = self.__convert_arc__(item, scale=scale)
                    list_lines += line_to_draw
                elif type(item) == QuadraticBezier:
                    line_to_draw = self.__convert_Quadratic_bezier__(item, scale=scale)
                    list_lines += line_to_draw
                else:
                    print(f'WARNING : non-supported type ({type(item)})')
        return(list_lines)

    def recommanded_ratio(self, list_lines):
        # Define a good ratio
        flattened_x1 = [val[0] for val in list_lines]
        flattened_x2 = [val[2] for val in list_lines]
        max_x = max(max(flattened_x1), max(flattened_x2))
        flattened_y1 = [val[1] for val in list_lines]
        flattened_y2 = [val[3] for val in list_lines]
        max_y = max(max(flattened_y1), max(flattened_y2))
        ratio_x = ratio_y = 1.0
        if max_x > 800:
            ratio_x = max_x / 800
        if max_y > 900:
            ratio_y = max_y / 900
        ratio = max(ratio_x, ratio_y)
        return ratio
