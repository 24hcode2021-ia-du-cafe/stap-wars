

An SVG file contain multiple 'Path' object

A 'Path' object contain one or more of one of those types :
* Line()
* Arc()
* CubicBezier()


## Line
Line.start ==> complex number for starting point
Line.end   ==> complex number for ending point
