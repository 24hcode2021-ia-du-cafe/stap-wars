from ctypes import *
import os

curent_directory = os.getcwd()


class NSVGpath(Structure):
    pass

NSVGpath._fields_ = [("pts", POINTER(c_float)),
                     ("npts", c_int),
                     ("closed", c_char),
                     ("bounds", c_float * 4),
                     ("next", POINTER(NSVGpath))]

class NSVGgradientStop(Structure):
    _fields_ = [("color", c_uint),
                ("offset", c_float)]

class NSVGgradient(Structure):
    _fields_ = [("xform", c_float * 6),
                ("spread", c_char),
                ("fx", c_float),
                ("fy", c_float),
                ("nstops", c_int),
                ("stops", NSVGgradientStop)]

class GradientWithColor(Union):
    _fields_ = [("color", c_uint),
                ("gradient", POINTER(NSVGgradient))]

class NSVGpaint(Structure):
    _fields_ = [("type", c_char),
                ("gradient_with_color", GradientWithColor)
    ]

class NSVGshape(Structure):
    pass


NSVGshape._fields_ = [("id", c_char * 64),
                      ("fill", NSVGpaint),
                      ("stroke", NSVGpaint),
                      ("opacity", c_float),
                      ("strokeWidth", c_float),
                      ("strokeDashOffset", c_float),
                      ("strokeDashArray", c_float * 8),
                      ("strokeDashCount", c_char),
                      ("strokeLineJoin", c_char),
                      ("strokeLineCap", c_char),
                      ("miterLimit", c_float),
                      ("fillRule", c_char),
                      ("flags", c_ubyte),
                      ("bounds", c_float * 4),
                      ("paths", POINTER(NSVGpath)),
                      ("next", POINTER(NSVGshape)),
                     ]

class NSVGimage(Structure):
    _fields_ = [("width", c_float),
                ("height", c_float),
                ("shapes", POINTER(NSVGshape))]


class NanoSvg:

    def __init__(self):
        self.my_dll = CDLL(f"{curent_directory}/libnanosvg.so")

    def nsvgParseFromFile(self, filename: str, units: str, dpi: float):
        self.my_dll.nsvgParseFromFile.restype = NSVGimage
        image = self.my_dll.nsvgParseFromFile(filename, units, c_float(dpi))
        return image

nanosvg = NanoSvg()
image = nanosvg.nsvgParseFromFile('../polargraph/svg/epreuve4.svg', 'px', 96)
print(f'Image size = ({image.width}, {image.height}')
shape = image.shapes.contents
print(shape)
shape = shape.next.contents
print(shape)
path = shape.npts
print(path)
