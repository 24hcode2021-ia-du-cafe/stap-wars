import sys
sys.path.append('../lib_svgfile')
from lib_svgfile import svgFile

svgfile = svgFile('../polargraph/svg/epreuve4.svg')
list_lines = svgfile.convert_to_list(scale=1.0)

# Launch the drawing
sys.path.append('../lib_xwing')
from lib_xwing import XWing
xwing = XWing()
previous_x = None
previous_y = None
for line in list_lines:
    if (previous_x != line[0]) or (previous_y != line[1]):
        xwing.pen_up()
        xwing.goto(line[0], line[1])
        xwing.pen_down()
    xwing.draw_line(line[2], line[3])
    previous_x = line[2]
    previous_y = line[3]


