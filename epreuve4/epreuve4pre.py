import sys
import csv
sys.path.append('../lib_svgfile')
from lib_svgfile import svgFile
from sys import argv
import pandas as pd

# parse SVG
svgfile = svgFile(argv[1])
#svgfile = svgFile('../polargraph/svg/epreuve4.svg')
list_lines = svgfile.convert_to_list(scale=1.0)



#tri 
# df = pd.DataFrame (list_lines, columns = ['start_x', 'start_y', 'end_x', 'end_y'])
# sorted_df = df.sort_values(by=['start_x', 'start_y'])
# list_lines2 = sorted_df.values.tolist()

# list_lines3 = []
# for i,row in enumerate(list_lines2):
#     if i%2:
#         list_lines3.append([row[2], row[3], row[0], row[1]])
#     else:
#         list_lines3.append([row[0], row[1], row[2], row[3]])
#     #list_lines3.append([row[0], row[1], row[2], row[3]])

# save CSV
fichier = open("svg.csv", "wt")
fichierCSV = csv.writer(fichier,delimiter=";")
for row in list_lines:
    fichierCSV.writerow(row)
fichier.close()

# print('')
# print(f'List of lines = {list_lines}')
# print('')
# print(svgfile.attributes)
# print('')
# print(svgfile.svg_attributes)
# print('')


