import sys
import csv

list_lines = []
fichier = open("svg.csv", "rt")
lecteurCSV = csv.reader(fichier,delimiter=";")
for row in lecteurCSV:
    coordonnees = []
    for coordonnee in row:
        coordonnees.append(float(coordonnee))
    list_lines.append(coordonnees)
fichier.close()

# Launch the drawing
sys.path.append('../lib_xwing')
from lib_xwing import XWing
xwing = XWing()
previous_x = None
previous_y = None
for line in list_lines:
    if (previous_x != line[0]) or (previous_y != line[1]):
        xwing.pen_up()
        xwing.goto(line[0], line[1])
        xwing.pen_down()
    xwing.draw_line(line[2], line[3])
    previous_x = line[2]
    previous_y = line[3]
    
xwing.goto(xwing.CENTER_WIDTH, xwing.CENTER_HEIGHT)
