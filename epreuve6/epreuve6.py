import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

import sys
sys.path.append('../lib_xwing')
from lib_xwing import XWing

import csv

class MyWindow(Gtk.Window):

    def __init__(self):
        super().__init__(title="Hello ST")

        self.set_border_width(10)
        #dialog.set_default_size(800, 400)
        
        self.pened = False

        box = Gtk.Box(spacing=6)
        self.add(box)

        button1 = Gtk.Button(label="RUN SVG")
        button1.connect("clicked", self.on_run_clicked)
        box.add(button1)

    def on_run_clicked(self, widget):
        list_lines = []
        fichier = open("svg.csv", "rt")
        lecteurCSV = csv.reader(fichier,delimiter=";")
        for row in lecteurCSV:
            coordonnees = []
            for coordonnee in row:
                coordonnees.append(float(coordonnee))
            list_lines.append(coordonnees)
        fichier.close()

        # Launch the drawing
        sys.path.append('../lib_xwing')
        from lib_xwing import XWing
        xwing = XWing()
        previous_x = None
        previous_y = None
        for line in list_lines:
            if (previous_x != line[0]) or (previous_y != line[1]):
                xwing.pen_up()
                xwing.goto(line[0], line[1])
                xwing.pen_down()
            xwing.draw_line(line[2], line[3])
            previous_x = line[2]
            previous_y = line[3]
            
        xwing.goto(xwing.CENTER_WIDTH, xwing.CENTER_HEIGHT)
        


print("app stap wars starting ... ")
xwing = XWing()
win = MyWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
print("start ok ! ")