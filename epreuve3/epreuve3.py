import sys
sys.path.append('../lib_xwing')
from lib_xwing import XWing

xwing = XWing()

xwing.goto(xwing.CENTER_WIDTH-450/2, xwing.CENTER_HEIGHT-450/2)
xwing.pen_down()
xwing.draw_line(xwing.CENTER_WIDTH+450/2, xwing.CENTER_HEIGHT-450/2, 4.0)
xwing.draw_line(xwing.CENTER_WIDTH+450/2, xwing.CENTER_HEIGHT+450/2, 4.0)
xwing.draw_line(xwing.CENTER_WIDTH-450/2, xwing.CENTER_HEIGHT+450/2, 4.0)
xwing.draw_line(xwing.CENTER_WIDTH-450/2, xwing.CENTER_HEIGHT-450/2, 4.0)
xwing.pen_up()
xwing.goto(xwing.CENTER_WIDTH, xwing.CENTER_HEIGHT)