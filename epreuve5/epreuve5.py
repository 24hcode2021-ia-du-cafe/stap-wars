import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

import sys
sys.path.append('../lib_xwing')
from lib_xwing import XWing

class MyWindow(Gtk.Window):

    def __init__(self):
        super().__init__(title="Hello ST")

        self.set_border_width(10)
        self.maximize()
        self.pened = False

        grid = Gtk.Grid(column_spacing=5,row_spacing=5)
        grid.set_hexpand(True)
        grid.set_vexpand(True)
 

        # self.box = Gtk.Box(spacing=6)

        # flowbox = Gtk.FlowBox()
        # flowbox.set_valign(Gtk.Align.START)
        # flowbox.set_max_children_per_line(3)
        # flowbox.set_selection_mode(Gtk.SelectionMode.NONE)

        # scrolled = Gtk.ScrolledWindow()
        # scrolled.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)

        # scrolled.add(flowbox)

        # self.add(scrolled)

        button_1 = Gtk.Button(label='     haut    ', expand = True)
        button_1.connect("clicked", self.on_up_clicked)
        #image = gtk.Image.new_from_file("up.PNG")
        #button_1.add(image)
        button_2 = Gtk.Button(label='     gauche    ', expand = True)
        button_2.connect("clicked", self.on_left_clicked)
        button_3 = Gtk.Button(label='     droite    ', expand = True)
        button_3.connect("clicked", self.on_right_clicked)
        button_4 = Gtk.Button(label='     bas       ', expand = True)
        button_4.connect("clicked", self.on_down_clicked)

        button_5 = Gtk.Button(label='    home    ', expand = True)
        button_5.connect("clicked", self.on_home_clicked)

        button_6 = Gtk.Button(label='    pen down    ', expand = True)
        button_6.connect("clicked", self.on_pen_clicked)

        # button_0 = Gtk.Button(label='')

        # flowbox.add(button_0)
        # flowbox.add(button_1)
        # flowbox.add(button_0)
        # flowbox.add(button_2)
        # flowbox.add(button_0)
        # flowbox.add(button_3)
        # flowbox.add(button_0)
        # flowbox.add(button_4)
        # flowbox.add(button_0)
        # flowbox.add(button_5)

        # self.add(button_1)
        # self.add(button_2)
        # self.add(button_3)
        # self.add(button_4)
        # self.add(button_5)


        grid.attach(button_1, 4, 0, 4, 4) 
        grid.attach(button_2, 0, 4, 4, 4)
        grid.attach(button_3, 8, 4, 4, 4)
        grid.attach(button_4, 4, 8, 4, 4)
        grid.attach(button_5, 0, 12, 4, 4)
        grid.attach(button_6, 8, 12, 4, 4)

        self.add(grid)

        #self.button = Gtk.Button(label="")
        #self.button.connect("clicked", self.on_button_clicked)
        #self.add(self.button)

    def on_up_clicked(self, widget):
        print("up clicked !")
        if self.pened == True:
            xwing.draw_line(xwing.x, xwing.y -10, 1.0)
        else:
            xwing.goto(xwing.x, xwing.y -10)

    def on_left_clicked(self, widget):
        print("left clicked !")
        if self.pened == True:
            xwing.draw_line(xwing.x -10, xwing.y , 1.0)
        else:
            xwing.goto(xwing.x -10, xwing.y)

    def on_right_clicked(self, widget):
        print("right clicked !")
        if self.pened == True:
            xwing.draw_line(xwing.x +10, xwing.y , 1.0)
        else:
            xwing.goto(xwing.x +10, xwing.y)
        
    def on_down_clicked(self, widget):
        print("down clicked !")
        if self.pened == True:
            xwing.draw_line(xwing.x, xwing.y +10, 1.0)
        else:
            xwing.goto(xwing.x, xwing.y +10)

    def on_home_clicked(self, widget):
        print("home clicked !")
        xwing.goto(xwing.CENTER_WIDTH, xwing.CENTER_HEIGHT)

    def on_pen_clicked(self, widget):
        print("pen clicked !")
        if self.pened == True:
            xwing.pen_up()
            widget.set_label('    pen down    ')
        else :
            xwing.pen_down()
            widget.set_label('    pen up    ')
        self.pened = not self.pened

print("app stap wars starting ... ")
xwing = XWing()
win = MyWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
print("start ok ! ")