#include <stdlib.h>
#include "pen.h"
#include "wires.h"

int main(int argc, char *argv[]) {
	printf("hello world\n");
	pen_up();
	sleep(5);
	float wireL = 0;
	float wireR = 0;
	wires_mm(0, 100, &wireL, &wireR);
	sleep(5);
	pen_down();
	return 0;
}
