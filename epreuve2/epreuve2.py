import sys
sys.path.append('../lib_xwing')
from lib_xwing import XWing
import time

xwing = XWing()

xwing.pen_up()
time.sleep(0.5)
xwing.pen_down()
time.sleep(0.5)
xwing.move(100, 100)