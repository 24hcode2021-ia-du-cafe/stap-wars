from ctypes import *
import os

curent_directory = os.getcwd()

class XWing:

    def __init__(self):
        self.MIN_HEIGHT = 0
        self.MIN_WIDTH = 0
        self.MAX_HEIGHT = 900 * 0.6
        self.MAX_WIDTH = 800 * 0.6
        self.CENTER_HEIGHT = self.MAX_HEIGHT / 2
        self.CENTER_WIDTH = self.MAX_WIDTH / 2
        self.my_dll = CDLL(f"{curent_directory}/../polargraph/libpolargraph.so")
        self.__draw_init__()

    def pen_up(self):
        self.my_dll.pen_up()

    def pen_down(self):
        self.my_dll.pen_down()

    def __move__(self, left: float, right: float):
        '''
        Move the xwing

        Parameters
        ----------
        left : float
            The distance to move in millimeter for the left motor
            Positive value if down, negative value is up
        right : float
            The distance to move in millimiter for the right motor
            Positive value if down, negative value is up
        
        Returns
        -------
        status : int
            0 if everything went ok
            error code otherwise
        '''
        returned_left = c_float()
        returned_right = c_float()
        status = self.my_dll.wires_mm(c_float(left), c_float(right),
                                      byref(returned_left), byref(returned_right))
        return status

    def __draw_init__(self):
        '''
        Initialize the drawing area
        
        Returns
        -------
        status : int
            0 if everything went ok
            error code otherwise
        '''
        # TODO : shouldn't we include this in the init method ?

        status = self.my_dll.draw_init()
        self.x = self.CENTER_HEIGHT
        self.y = self.CENTER_WIDTH
        return status

    def get_drawing_area(self):
        '''
        Get information about the drawing area
        
        Returns
        -------
        x : float
        y : float
        width : float
        height : float
        '''
        x = c_float()
        y = c_float()
        width = c_float()
        height = c_float()
        status = self.my_dll.draw_get_drawing_area(byref(x), byref(y),
                                                   byref(width), byref(height))
        return x, y, width, height

    def goto(self, x: float, y: float, step_mm :float = 10.0):
        '''
        Move the xwing from anywhere to the x and y coordinates

        Parameters
        ----------
        x : float
        y : float
        step_mm : float, default 10.0
        
        Returns
        -------
        status : int
            0 if everything went ok
            error code otherwise
        '''

        initial_x = self.x
        initial_y = self.y
        # Calculate the number of iteration to do
        nb_iterations = max(int(abs(x - initial_x)), int(abs(y - initial_y)))
        # Scale nb iter with step lenght
        nb_iterations = int(nb_iterations / step_mm)
        # Iterate each small increment
        if nb_iterations > 0:
            step_x = (x - initial_x) / float(nb_iterations)
            step_y = (y - initial_y) / float(nb_iterations)
            for i in range(nb_iterations):
                new_x = initial_x + (step_x * (i + 1))
                new_y = initial_y + (step_y * (i + 1))
                status = self.my_dll.draw_goto(c_float(new_x), c_float(new_y))
        # Do the last small move
        status = self.my_dll.draw_goto(c_float(x), c_float(y))
        self.x = x
        self.y = y
        return status

    def draw_line(self, x: float, y: float, step_mm :float = 2.0):
        '''
        Draw a line with the xwing from the current position
        to the x and y coordinates

        Parameters
        ----------
        x : float
        y : float
        step_mm : float, default 2.0
        
        Returns
        -------
        status : int
            0 if everything went ok
            error code otherwise
        '''
        # We need to move by small iteration to have a beautiful line
        # Get the current position
        initial_x = self.x
        initial_y = self.y
        # Calculate the number of iteration to do
        nb_iterations = max(int(abs(x - initial_x)), int(abs(y - initial_y)))
        # Scale nb iter with step lenght
        nb_iterations = int(nb_iterations / step_mm)
        # Iterate each small increment
        if nb_iterations > 0:
            step_x = (x - initial_x) / float(nb_iterations)
            step_y = (y - initial_y) / float(nb_iterations)
            for i in range(nb_iterations):
                new_x = initial_x + (step_x * (i + 1))
                new_y = initial_y + (step_y * (i + 1))
                status = self.my_dll.draw_line(c_float(new_x), c_float(new_y))
        # Do the last small move
        status = self.my_dll.draw_line(c_float(x), c_float(y))
        # Update the current coordinates
        self.x = x
        self.y = y
        return status

    def home(self):
        '''
        Return from anywhere to the 'home' of the drawing area
        
        Returns
        -------
        status : int
            0 if everything went ok
            error code otherwise
        '''
        status = self.my_dll.draw_home()
        return status
