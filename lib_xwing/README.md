# lib_xwing

## Objective

Wrapper to the libpolargraph.so provided by ST Microelectronic for the 2021 24h code challenge
The C library is needed, the path is harcoded in the wrapper


## Init the library

`import lib_xwing`

`xwing = lib_xwing.XWing()`

You can now call the methods on your new xwing object.


## Exposed methods

### pen-up()

Set the pen in the upward position (not drawing)

Example :

`xwing.pen_up()`


### pen-down()

Set the pen in the downward position (drawing)

Example :

`xwing.pen_down()`


### draw_init

Init the drawing board

Example :
`xwing.draw_init()`


### get_drawing_area

Get information about the drawing area

Example :
`x, y, width, height = xwing.get_drawing_area()`

### goto

Move the xwing from anywhere to the x and y coordinates

Example :
`status = xwing.goto(12,15)`
Move the xwing (without drawing) from where it is to the (12, 15) coordinates


### draw_line

Draw a line from current position to the x and y coordinates

Example :
`status = xwing.draw_line(13,56)`
Draw a line from current position to the (13, 56) coordinates


### home

Move the xwing from anywhere to the (0, 0) coordinate

Example :
`status = xwing.home()`
